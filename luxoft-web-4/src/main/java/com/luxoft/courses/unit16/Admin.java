package com.luxoft.courses.unit16;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicLong;


public class Admin extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("Active Sessions="+((AtomicLong)getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE)).toString()+"<br>");
        out.println("Active User Sessions="+((AtomicLong)getServletContext().getAttribute(Constants.ACTIVE_USER_SESSION_ATTRIBUTE)).toString()+"<br>");
        out.println("Active Admin Sessions="+((AtomicLong)getServletContext().getAttribute(Constants.ACTIVE_ADMIN_SESSION_ATTRIBUTE)).toString()+"<br>");
        out.println("Total count of Http requests="+((AtomicLong)getServletContext().getAttribute(Constants.HTTP_REQUEST_COUNT)).toString()+"<br>");
        out.println("Total count of POST requests="+((AtomicLong)getServletContext().getAttribute(Constants.HTTP_POST_REQUEST_COUNT)).toString()+"<br>");
        out.println("Total count of GET requests="+((AtomicLong)getServletContext().getAttribute(Constants.HTTP_GET_REQUEST_COUNT)).toString()+"<br>");
        out.println("Total count of OTHER requests="+((AtomicLong)getServletContext().getAttribute(Constants.HTTP_OTHER_REQUEST_COUNT)).toString()+"<br>");
        out.println("</html>");
    }
}
