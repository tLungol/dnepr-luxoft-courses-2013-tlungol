package com.luxoft.courses.unit16;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;


public class RequstListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        ((AtomicLong) servletRequestEvent.getServletContext().
                getAttribute(Constants.HTTP_REQUEST_COUNT)).getAndIncrement();
        if(((HttpServletRequest) servletRequestEvent.getServletRequest()).getMethod().equals("GET")){
            ((AtomicLong) servletRequestEvent.getServletContext().
                    getAttribute(Constants.HTTP_GET_REQUEST_COUNT)).getAndIncrement();
        }else if(((HttpServletRequest) servletRequestEvent.getServletRequest()).getMethod().equals("POST")){
            ((AtomicLong) servletRequestEvent.getServletContext().
                    getAttribute(Constants.HTTP_POST_REQUEST_COUNT)).getAndIncrement();
        }else {
            ((AtomicLong) servletRequestEvent.getServletContext().
                    getAttribute(Constants.HTTP_OTHER_REQUEST_COUNT)).getAndIncrement();
        }
    }

}
