package com.luxoft.courses.unit16;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        if (session == null) {
            response.sendRedirect("/myFirstWebApp/index.jsp");
        } else if (session.getAttribute("role")!=null && ((String) session.getAttribute("role")).equals("user")) {
            response.sendRedirect("/myFirstWebApp/user");
        }
        filterChain.doFilter(request, response);
    }


    @Override
    public void destroy() {

    }
}
