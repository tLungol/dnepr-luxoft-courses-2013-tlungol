package com.luxoft.courses.unit16;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;


public class User extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        PrintWriter out = response.getWriter();
        if (session!=null){

            if(request.getQueryString()!=null&&request.getQueryString().equals("logout")){
                session.invalidate();
                response.sendRedirect("index.html");
                return;
            }

            if (session.getAttribute("login")!=null){
                out.println("<html><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>" +
                        "<div class=\"Login\">Hello "+session.getAttribute("login")+"!</div>" +
                        "<div class\"logout\"><a href='user?logout'>logout</a></div>" +"</html>");
                return;
            }

        }

    }
}
