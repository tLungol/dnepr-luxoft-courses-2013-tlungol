package com.luxoft.courses.unit16;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
            String role;
        if ((role =check(request.getParameter("name"), request.getParameter("password")))!=null) {
            HttpSession session = request.getSession();
            session.setAttribute("role",role);
            session.setAttribute("login",request.getParameter("name"));
            if (role.equals("user")){
                ((AtomicLong)getServletContext().
                        getAttribute(Constants.ACTIVE_USER_SESSION_ATTRIBUTE)).getAndIncrement();
            } else {
                ((AtomicLong)getServletContext().
                        getAttribute(Constants.ACTIVE_ADMIN_SESSION_ATTRIBUTE)).getAndIncrement();
            }
            out.print(role);
        } else {
            out.print("Wrong Login or password!!!");
        }

    }

    private boolean checkParam(String param) {
        if (param != null && param != "")
            return true;
        return false;
    }

    private String check(String userName, String pass) {

        Gson gson = new Gson();
        Type list = new TypeToken<List<SystemUser>>() {}.getType();
        ArrayList<SystemUser> users =
                gson.fromJson(getServletContext().getInitParameter("users"), list);
        for (SystemUser user : users) {
            if (checkParam(userName) && checkParam(pass) &&
                    user.getLogin().equals(userName) && user.getPassword().equals(pass)) {
                return user.getRole();
            }
        }
        return null;
    }
}
