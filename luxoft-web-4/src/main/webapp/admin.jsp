<%--
  Created by IntelliJ IDEA.
  User: bulick
  Date: 21.06.13
  Time: 8:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<html>
<head>
    <title>Admin</title>
</head>
<body>
    <table>
        <tr><td>Active Sessions</td><td>${ACTIVE_SESSION}</td></tr>
        <tr><td>Active Admin Sessions</td><td>${ACTIVE_ADMIN_SESSION}</td></tr>
        <tr><td>Active User Sessions</td><td>${ACTIVE_USER_SESSION}</td></tr>
        <tr><td>Http request count</td><td>${HTTP_REQUEST_COUNT}</td></tr>
        <tr><td>Http GET request count</td><td>${HTTP_GET_REQUEST_COUNT}</td></tr>
        <tr><td>Http POST request count</td><td>${HTTP_POST_REQUEST_COUNT}</td></tr>
        <tr><td>Http OTHER request count</td><td>${HTTP_OTHER_REQUEST_COUNT}</td></tr>
    </table>

</body>
</html>