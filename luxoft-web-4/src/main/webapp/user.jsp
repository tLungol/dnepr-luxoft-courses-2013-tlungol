
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<% if(request.getQueryString()!=null&&request.getQueryString().equals("logout")){
    session.invalidate();
    response.sendRedirect("index.html");
    return;
} %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>User</title>
</head>
<body>
<div class="Login">Hello ${sessionScope.login} !</div>
<div class="logout"><a href='user?logout'>logout</a></div>
</body>
</html>
