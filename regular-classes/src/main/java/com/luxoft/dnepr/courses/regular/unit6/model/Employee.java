package com.luxoft.dnepr.courses.regular.unit6.model;


public class Employee extends Entity{
    private int salary;

    public Employee(int salary) {
        this.salary = salary;
    }

    public Employee(Long id,int salary) {

        super(id);
        this.salary = salary;
    }

    public int getSalary() { return salary; }
    public void setSalary(int salary) { this.salary = salary; }
}
