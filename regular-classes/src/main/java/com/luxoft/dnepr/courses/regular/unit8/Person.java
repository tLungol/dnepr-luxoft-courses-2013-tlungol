package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Person implements Externalizable {
    
    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;
    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy h:mm:ss a",Locale.UK);
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
    
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        try {
            out.writeBytes("{");
            out.writeBytes(this.getName() == null ? "" : "\"name\":\"" + this.getName() + "\"");
            out.writeBytes(this.getGender() == null ? "" : ",\"gender\":\"" + this.getGender() + "\"");
            out.writeBytes(this.getEthnicity() == null ? "" : ",\"ethnicity\":\"" + this.getEthnicity()+"\"");
            out.writeBytes(this.getBirthDate() == null ? "" : ",\"birthDate\":\"" + dateFormat.format(this.getBirthDate()) + "\"");
            if (this.getFather()!=null){
                out.writeBytes(",\"father\":");
                this.getFather().writeExternal(out);
            }
            if (this.getMother()!=null){
                out.writeBytes(",\"mother\":");
                this.getMother().writeExternal(out);
            }
            out.writeBytes("}");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
