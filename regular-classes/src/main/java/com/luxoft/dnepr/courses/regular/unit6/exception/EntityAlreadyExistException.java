package com.luxoft.dnepr.courses.regular.unit6.exception;


public class EntityAlreadyExistException extends RuntimeException{
    public EntityAlreadyExistException(Long id) {
        super("User "+id+" already exist!");
    }
}
