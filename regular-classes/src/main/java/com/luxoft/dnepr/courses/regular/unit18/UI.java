package com.luxoft.dnepr.courses.regular.unit18;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class UI implements Runnable {
    public static String HELP_STRING = "Lingustic analizator v1, autor Tushar Brahmacobalol for Help type Help," +
            " answer y/n question, your english knowlege will give you";
    public static String ERROR_READING_ANSWER = "Error while reading answer";

    private static Vocabulary vocabulary = new Vocabulary();
    Boolean a = null;

    public static final Map<String, MyAction> ACTIONS;
    static {
        Map<String, MyAction> result = new HashMap<>();
        result.put("help", new Help());
        ACTIONS = Collections.unmodifiableMap(result);
    }

    public void run() {
        String word, answer = "";
        Scanner s = new Scanner(System.in);
        for (; ; ) {
            word = printWordFromVocabulary(vocabulary);
            answer = s.next();
            if (isNull(answer)) break;
            if  (isEmpty(answer))continue;
            if (!checkAnswer(answer,word)){
                break;
            }
        }
    }

    private static String printWordFromVocabulary(Vocabulary vocab) {
        String word = vocab.random();
        System.out.println("Do you know translation of this word?:");
        System.out.println(word);
        return word;
    }

    private static boolean checkAnswer(String answer, String word) {
        Boolean isAnswer;
        if (answer.equalsIgnoreCase("exit")) {
            Words.res(vocabulary.getSize());
            return false;
        } else if (answer.equalsIgnoreCase("Help")) {
            isEmpty("");
        } else if ((isAnswer = answer.equalsIgnoreCase("Y"))) {
            if (isAnswer == null) {
                System.out.println(ERROR_READING_ANSWER);
                return true;
            } else {
                Words.ans(word, isAnswer);
            }
        }
        return true;
    }

    private static boolean isEmpty(String answer) {
        if (answer.isEmpty()) {
            System.out.println(HELP_STRING);
        }
        return answer.isEmpty();
    }

    private static boolean isNull(String answer) {
        if (answer == null){
            System.out.println(ERROR_READING_ANSWER);
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        UI ui = new UI();
        ui.run();
    }
}
