package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct {

    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) ? weight == ((Bread) o).getWeight() : false;
    }


    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (int) (this.weight == 0 ? 0 : this.weight);
        return result;
    }
}
