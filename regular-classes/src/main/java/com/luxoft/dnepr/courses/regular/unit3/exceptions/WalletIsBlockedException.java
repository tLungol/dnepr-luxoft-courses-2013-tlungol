package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletIsBlockedException extends Exception {
    private Long walletId;

    public void setWalletId(Long walletId) {
        this.walletId = walletId;
    }

    public WalletIsBlockedException(Long walletId, String message) {

        super(message);
        this.walletId = walletId;
    }
}
