package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    List<Product> products = new ArrayList<Product>();
    CompositeProduct books = new CompositeProduct();
    CompositeProduct breadComposite = new CompositeProduct();
    CompositeProduct beverages = new CompositeProduct();

    private void sortDestinct() {

        Collections.sort(products, new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return (int) (o2.getPrice() - o1.getPrice());
            }
        });

    }

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */

    public void append(Product product) {
        if (product instanceof Book) {
            append((Book) product);
        }
        if (product instanceof Beverage) {
            append((Beverage) product);
        }
        if (product instanceof Bread) {
            append((Bread) product);
        }
    }

    public void append(Book product) {

        Book book = null;
        try {
            book = (Book) product.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        books.add(book);
        if (products.contains(product)) {
            products.remove(product);
            book.setPrice(books.getPrice());
            products.add(book);
        } else {
            products.add(product);
        }

        sortDestinct();
    }

    public void append(Bread product) {
        Bread bread = null;
        try {
            bread = (Bread) product.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        breadComposite.add(product);
        if (products.contains(product)) {
            products.remove(product);
            bread.setPrice(breadComposite.getPrice());
            products.add(bread);
        } else {
            products.add(product);

        }
        sortDestinct();
    }

    public void append(Beverage product) {
        Beverage beverage = null;
        try {
            beverage = (Beverage) product.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        beverages.add(product);
        if (products.contains(product)) {
            products.remove(product);
            beverage.setPrice(beverages.getPrice());
            products.add(beverage);
        } else {
            products.add(product);

        }
        sortDestinct();
    }


    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    public double summarize() {

        return breadComposite.getPrice() + beverages.getPrice() + books.getPrice();

    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize();
    }

}
