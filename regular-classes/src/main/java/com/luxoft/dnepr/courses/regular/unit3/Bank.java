package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;


public class Bank implements BankInterface {
    private Map<Long, UserInterface> users;


    public Bank(String expectedJavaVersion) {
        if (!System.getProperty("java.version").equals(expectedJavaVersion)) {
            throw new IllegalJavaVersionError(System.getProperty("java.version"), expectedJavaVersion, "Wrong JAVA version");
        }
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    private void checkUser(Long userId) throws NoUserFoundException, TransactionException {
        if (!users.containsKey(userId)) {
            throw new NoUserFoundException(userId, "No such user found!");
        }

    }

    private void checkSenderWallet(User user, BigDecimal amount) throws TransactionException {
        try {
            user.getWallet().checkWithdrawal(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '" + user.getName() + "' wallet is blocked");
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException("User '" + user.getName() + "' has insufficient funds (" +
                    currencyFormat(user.getWallet().getAmount()) +
                    " < " + currencyFormat(amount) + ")");
        }
    }


    private void checkReceiverWallet(User user, BigDecimal amount) throws TransactionException {
        try {
            user.getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '" + user.getName() + "' wallet is blocked");
        } catch (LimitExceededException e) {
            throw new TransactionException("User '" + user.getName() + "' wallet limit exceeded (" +
                    currencyFormat(user.getWallet().getAmount()) + " + " +
                    currencyFormat(amount) + " > " + currencyFormat(user.getWallet().getMaxAmount()) + ")");
        }
    }


    private static String currencyFormat(BigDecimal n) {

        NumberFormat form;
        form = NumberFormat.getCurrencyInstance(Locale.UK);
        ((DecimalFormat) form).applyPattern("0.00");
        return form.format(n);
    }

    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        checkUser(fromUserId);
        checkUser(toUserId);

        checkSenderWallet((User) users.get(fromUserId), amount);
        checkReceiverWallet((User) users.get(toUserId), amount);

        users.get(fromUserId).getWallet().withdraw(amount);
        users.get(toUserId).getWallet().transfer(amount);
    }


}
