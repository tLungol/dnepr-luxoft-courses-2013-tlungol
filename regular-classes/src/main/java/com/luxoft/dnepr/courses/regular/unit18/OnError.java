package com.luxoft.dnepr.courses.regular.unit18;

/**
 * Created with IntelliJ IDEA.
 * User: bulick
 * Date: 29.06.13
 * Time: 11:04
 * To change this template use File | Settings | File Templates.
 */
public class OnError implements MyAction{
    public static String ERROR_READING_ANSWER = "Error while reading answer";

    public OnError() {
        System.out.println(ERROR_READING_ANSWER);
    }
}
