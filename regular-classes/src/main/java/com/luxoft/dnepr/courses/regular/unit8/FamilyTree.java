package com.luxoft.dnepr.courses.regular.unit8;

import sun.misc.IOUtils;

import java.io.*;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FamilyTree implements Externalizable {

    private Person root;
    private transient Date creationTime;

    public FamilyTree() {
        setCreationTime(new Date());
    }

    public FamilyTree(Person root) {
        this();
        setRoot(root);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        String rootStr = in.readLine();
        Pattern p = Pattern.compile("\\{\"root\":\\{(.+)\\}");
        Matcher m = p.matcher(rootStr);
        m.find();
        rootStr = m.group(1);

        this.setRoot(readPerson(rootStr));


    }

    private Person readPerson(String str) {
        Person person = new Person();
        String sub = strRepleace(str);
        int begin;
        int fb;
        if ( (begin = str.indexOf('{'))!=-1){
            StringBuilder sb = new StringBuilder(str).reverse();
            if ((fb = str.indexOf("\"father\""))!=-1 && str.indexOf("\"father\"")<begin){
                String father = str.substring(fb+10,str.length()-(sb.indexOf("{:\"rehtom\",")+11));
                Pattern p = Pattern.compile("\"father\":\\{(.*?)\\}+");
                Matcher m = p.matcher(father);
                person.setFather(readPerson(father));

            }
            String mother = str.substring(str.length()-(sb.indexOf("{:\"rehtom\",")),str.length());
            person.setMother(readPerson(mother));

        }

        Pattern p = Pattern.compile("\"name\":\"(.*?)\"");
        Matcher m = p.matcher(str);
        if (m.find()) {
            person.setName(m.group(1));
        }



        p = Pattern.compile("\"gender\":\"(.*?)\"");
        m = p.matcher(sub);
        if (m.find()) {
            person.setGender(m.group(1).equals(Gender.MALE.toString()) ? Gender.MALE : Gender.FEMALE);

        }
        p = Pattern.compile("\"ethnicity\":\"(.*?)\"");
        m = p.matcher(sub);
        if (m.find()) {
            person.setEthnicity(m.group(1));

        }
        p = Pattern.compile("\"birthDate\":\"(.*?)\"");
        m = p.matcher(sub);
        if (m.find()) {
            try {
                person.setBirthDate(Person.dateFormat.parse(m.group(1)));
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }

        return person;
    }


    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeBytes("{\"root\":");
        this.getRoot().writeExternal(out);
        out.writeBytes("}");

    }

    public Person getRoot() {
        return root;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    private void setRoot(Person root) {
        this.root = root;
    }

    private String strRepleace(String str) {
        int end = 0;
        if (str.indexOf('{')==-1) return str;
        if (str.indexOf('{') - str.indexOf('}')<0) {
            end = str.indexOf('{');
        } else {
            end = str.indexOf('}')!=-1 ? str.indexOf('}') : str.length() ;
        }
        return str.substring(0, end);

    }

    private void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }
}
