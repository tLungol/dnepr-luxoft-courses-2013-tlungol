package com.luxoft.dnepr.courses.regular.unit3;


public class User implements UserInterface {
    private Long id;
    private static Long nextId = Long.valueOf(0);
    private String name;
    private Wallet wallet;

    private Long getNextId() {
        return ++nextId;
    }

    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public User() {
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public User(String name, Wallet wallet) {
        this.id = getNextId();
        setName(name);
        setWallet(wallet);
    }

    @Override
    public void setWallet(WalletInterface wallet) {
        if (wallet == null) {
            throw new IllegalArgumentException();
        }
        this.wallet = (Wallet) wallet;

    }


}
