package com.luxoft.dnepr.courses.regular.unit3;


import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;

public class Wallet implements WalletInterface {
    private long nextId = 0;
    private long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;


    @Override
    public Long getId() {
        return this.id;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public BigDecimal getAmount() {
        return this.amount;
    }

    public Wallet(WalletStatus status, BigDecimal maxAmount, BigDecimal amount) {
        this.id = ++nextId;
        this.status = status;
        this.maxAmount = maxAmount;
        this.amount = amount;
    }

    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public WalletStatus getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }

    @Override
    public BigDecimal getMaxAmount() {
        return this.maxAmount;
    }

    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        if (maxAmount == null || maxAmount.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new IllegalArgumentException();
        }
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw) throws WalletIsBlockedException, InsufficientWalletAmountException {
        if (this.status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(this.getId(), "wallet is blocked");
        }
        if (amountToWithdraw.compareTo(this.amount) > 0) {
            throw new InsufficientWalletAmountException(this.getId(), amountToWithdraw, this.getAmount(), "not enough money");
        }


    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        if (amountToWithdraw == null || amountToWithdraw.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new IllegalArgumentException();
        }
        this.amount = this.amount.subtract(amountToWithdraw);

    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {

        if (this.status.equals(WalletStatus.BLOCKED)) {
            throw new WalletIsBlockedException(this.getId(), "Wallet is BLOCKED");
        }

        if ((this.amount.add(amountToTransfer)).compareTo(this.maxAmount) > 0) {
            throw new LimitExceededException(this.id, amountToTransfer, this.amount, "out of limit");
        }

    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
        if (amountToTransfer == null || amountToTransfer.compareTo(BigDecimal.valueOf(0)) < 0) {
            throw new IllegalArgumentException();
        }
        this.amount = this.amount.add(amountToTransfer);

    }
}
