package com.luxoft.dnepr.courses.regular.unit18;

public class Words {
    public static java.util.Vector<String> knwn = new java.util.Vector();
    public static java.util.Vector<String> nknown = new java.util.Vector();

    public static void res(int vocabularySize) {
        int w = vocabularySize * (knwn.size() + 1) / (knwn.size() + nknown.size() + 1);
        System.out.println("Your estimated vocabulary is " + w + " words");
    }

    public static void ans(String word, Boolean know) {
        knwn.add(know ? word : "");
        nknown.add(!know ? word : "");
    }

}
