package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Product {
    private Date publicationDate;

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) ? this.publicationDate.equals(((Book) o).getPublicationDate()) : false;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Book cloned = (Book) super.clone();
        cloned.setPublicationDate((Date) this.publicationDate.clone());
        return cloned;
    }

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }
}
