package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Compiler {

    public static final boolean DEBUG = true;

    public static void main(String[] args) {
        byte[] byteCode = compile("3+(2+1)+4");
        VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
        vm.run();
    }


    static ByteArrayOutputStream retOperation(String str, ByteArrayOutputStream out) {
        switch (str.trim().charAt(0)) {
            case '-': {
                out.write(VirtualMachine.SWAP);
                out.write(VirtualMachine.SUB);
                break;
            }
            case '+': {
                out.write(VirtualMachine.ADD);
                break;
            }
            case '*': {
                out.write(VirtualMachine.MUL);
                break;
            }
            case '/': {
                out.write(VirtualMachine.SWAP);
                out.write(VirtualMachine.DIV);
                break;
            }

        }
        return out;
    }

    static void scopes(String str, Pattern pattern, ByteArrayOutputStream result) {
        Matcher matcher = pattern.matcher(str);
        if (matcher.find()) {
            scopes(matcher.group(), pattern, result);
        } else {
            push(result, matcher.group());

        }

    }

    public static void push(ByteArrayOutputStream result, String input) {

        scopes(input, Pattern.compile("(?<=\\()([\\d\\D]+)(?=\\))"), result);
        String[] numbers = input.split("\\Q-\\E|\\Q+\\E|\\Q/\\E|\\Q*\\E");
        String[] operations = input.split("[0-9.]+");
        result.write(VirtualMachine.PUSH);
        writeDouble(result, Double.parseDouble(numbers[0]));
        result.write(VirtualMachine.PUSH);
        writeDouble(result, Double.parseDouble(numbers[1]));
        try {
            result.write(retOperation(operations[1], result).toByteArray());
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        for (int i = 2; i < numbers.length; i++) {
            result.write(VirtualMachine.PUSH);
            writeDouble(result, Double.parseDouble(numbers[i]));
            try {
                result.write(retOperation(operations[i], result).toByteArray());
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }

    }


    static byte[] compile(String input) {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
    /*    double a = 0;
        double b = 0;
        char operation = 0;
        String[] numbers = input.split("\\Q-\\E|\\Q+\\E|\\Q/\\E|\\Q*\\E");
        String[] operations = input.split("[0-9.]+");

        result.write(VirtualMachine.PUSH);
        writeDouble(result, Double.parseDouble(numbers[0]));
        result.write(VirtualMachine.PUSH);
        writeDouble(result, Double.parseDouble(numbers[1]));
        try {
            result.write(retOperation(operations[1], result).toByteArray());
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        for (int i = 2; i < numbers.length; i++) {
            result.write(VirtualMachine.PUSH);
            writeDouble(result, Double.parseDouble(numbers[i]));
            try {
                result.write(retOperation(operations[i], result).toByteArray());
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }*/

        push(result, input);

        result.write(VirtualMachine.PRINT);
        return result.toByteArray();
    }

    /**
     * Adds specific command to the byte stream.
     *
     * @param result
     * @param command
     */
    public static void addCommand(ByteArrayOutputStream result, byte command) {
        result.write(command);
    }

    /**
     * Adds specific command with double parameter to the byte stream.
     *
     * @param result
     * @param command
     * @param value
     */
    public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
        result.write(command);
        writeDouble(result, value);
    }

    private static void writeDouble(ByteArrayOutputStream result, double val) {
        long bits = Double.doubleToLongBits(val);

        result.write((byte) (bits >>> 56));
        result.write((byte) (bits >>> 48));
        result.write((byte) (bits >>> 40));
        result.write((byte) (bits >>> 32));
        result.write((byte) (bits >>> 24));
        result.write((byte) (bits >>> 16));
        result.write((byte) (bits >>> 8));
        result.write((byte) (bits >>> 0));
    }

    private static String getInputString(String[] args) {
        if (args.length > 0) {
            return join(Arrays.asList(args));
        }

        Scanner scanner = new Scanner(System.in);
        List<String> data = new ArrayList<String>();
        while (scanner.hasNext()) {
            data.add(scanner.next());
        }
        return join(data);
    }


    private static String join(List<String> list) {
        StringBuilder result = new StringBuilder();
        for (String element : list) {
            result.append(element);
        }
        return result.toString();
    }

}
