package com.luxoft.dnepr.courses.regular.unit18;

import java.io.*;
import java.util.HashSet;
import java.util.StringTokenizer;

import org.apache.commons.io.*;

public class Vocabulary {

    public static String FILE_READ_ERROR = "ERROR: Read from file FAILED!";

    private HashSet<String> voc = new HashSet<String>();

    public int getSize() {
        return voc.size();
    }

    public Vocabulary() {
        readVocabularyFromFile(getClass().getResourceAsStream("sonnets.txt"));
    }

    public String random() {
        return voc.toArray(new String[]{})[(int) (Math.random() * voc.size())];
    }

    private void readVocabularyFromFile(InputStream in) {
        String vocabularyString = null;
        try {
            vocabularyString = IOUtils.toString(in);
        } catch (IOException e) {
            System.out.println(FILE_READ_ERROR);
        }

        tokenizeVocabulary(vocabularyString);

    }

    private void tokenizeVocabulary(String vocabularyString) {
        StringTokenizer t = new StringTokenizer(vocabularyString);
        while (t.hasMoreTokens()) {
            String word = t.nextToken();
            if (word.length() > 3) {
                voc.add(word.toLowerCase());
            }
        }
    }
}
