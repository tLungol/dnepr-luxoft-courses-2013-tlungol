package com.luxoft.dnepr.courses.regular.unit6.storage;


import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage.getEntities;

public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();
    public static Lock lock = new ReentrantLock();

    private EntityStorage() {
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }

    public static Long getNextId() {
        lock.lock();
        try{
            Iterator<Long> i = entities.keySet().iterator();
            Long max = i.next();
            Long next;
            while (i.hasNext()) {
                next = i.next();
                if (next.compareTo(max) > 0) {
                    max = next;
                }
            }
            return max + 1;
        }
        finally {
            lock.unlock();
        }


    }

}

