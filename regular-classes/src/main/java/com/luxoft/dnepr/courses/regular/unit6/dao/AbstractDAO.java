package com.luxoft.dnepr.courses.regular.unit6.dao;


import com.luxoft.dnepr.courses.regular.unit6.exception.EntityAlreadyExistException;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;
import com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage;

import java.util.Iterator;

import static com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage.*;

public abstract class AbstractDAO<E extends Entity> implements IDao{

    @Override
    public Entity save(Entity entity) throws EntityAlreadyExistException {
        EntityStorage.lock.lock();
        try {
            if (getEntities().containsKey(entity.getId())){
                throw new EntityAlreadyExistException(entity.getId());
            }
            if (entity.getId() == null) {
                Long nextID= EntityStorage.getNextId();
                entity.setId(nextID);
            }
            getEntities().put(entity.getId(),entity);
        }finally {
            EntityStorage.lock.unlock();
        }

        return entity;
    }

    @Override
    public Entity update(Entity entity) throws EntityNotFoundException {
        EntityStorage.lock.lock();
        try {
            if (getEntities().containsKey(entity.getId())) {
                getEntities().put(entity.getId(), entity);
                return entity;
            }
            throw new EntityNotFoundException();
        }finally {
            EntityStorage.lock.unlock();
        }


    }

    @Override
    public Entity get(long id) {
        return getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        EntityStorage.lock.lock();
        try {
            if ((getEntities().remove(id))==null){
                return false;
            }

        }finally {
            EntityStorage.lock.unlock();
        }
        return true;
    }



}
