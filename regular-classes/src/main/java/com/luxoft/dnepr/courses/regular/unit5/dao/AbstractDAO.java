package com.luxoft.dnepr.courses.regular.unit5.dao;


import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.Iterator;

import static com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage.*;

public abstract class AbstractDAO<E extends Entity> implements IDao{

    @Override
    public Entity save(Entity entity) throws UserAlreadyExist {
        if (getEntities().containsKey(entity.getId())){
            throw new UserAlreadyExist(entity.getId());
        }
        if (entity.getId() == null) {
            Long nextID=getMaxId()+1;
            entity.setId(nextID);
        }
        getEntities().put(entity.getId(),entity);
        return entity;
    }

    @Override
    public Entity update(Entity entity) throws UserNotFound {
        if (getEntities().containsKey(entity.getId())) {
            getEntities().put(entity.getId(), entity);
            return entity;
        }
        throw new UserNotFound();

    }

    @Override
    public Entity get(long id) {
        return getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        if ((getEntities().remove(id))==null){
            return false;
        }
        return true;
    }

    private Long getMaxId() {
        Iterator<Long> i= getEntities().keySet().iterator();
        Long max= i.next();
        Long next;
        while (i.hasNext()){
            next=i.next();
            if (next.compareTo(max)>0){
                max=next;
            }
        }
        return max;
    }

}
