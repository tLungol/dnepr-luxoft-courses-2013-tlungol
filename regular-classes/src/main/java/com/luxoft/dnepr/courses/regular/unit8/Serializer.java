package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Serializer {

    public static void serialize(File file, FamilyTree entity) {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))) {
            out.writeObject(entity);
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private static void writePerson(Person person, BufferedWriter bw) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy hh:mm:ss a");
        try {
            bw.write("{");
            bw.write(person.getName() == null ? "" : "\"name\":\"" + person.getName() + "\"");
            bw.write(person.getGender() == null ? "" : ",\"gender\":\"" + person.getGender() + "\"");
            bw.write(person.getEthnicity() == null ? "" : ",\"ethnicity\":\"" + person.getEthnicity() + "\"");
            bw.write(person.getBirthDate() == null ? "" : ",\"birthDate\":\"" + dateFormat.format(person.getBirthDate()) + "\"");
            if (person.getFather() != null) {
                bw.write(",\"father\":\"");
                writePerson(person.getFather(), bw);
            }
            if (person.getMother() != null) {
                bw.write(",\"mother\":\"");
                writePerson(person.getMother(), bw);
            }
            bw.write("}");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public static FamilyTree deserialize(File file) {
        FamilyTree tree=null;
        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(file))) {
            tree = (FamilyTree) in.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return tree;
    }
}
