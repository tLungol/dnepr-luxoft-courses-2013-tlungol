package com.luxoft.dnepr.courses.regular.unit7_1;


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class ThreadProducer {
    private ThreadProducer() {
    }

    public static Lock lock = new ReentrantLock();

    public final static class SynchronizedCounter {
        private static int c = 0;

        public static synchronized void increment() {
            for (; ; ) {
                c++;
                c--;
            }

        }

    }

    public static Thread getNewThread() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        return t;
    }

    public static Thread getRunnableThread() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                for (; ; ) {
                    i++;
                    i--;
                }
            }
        });
        t.start();
        return t;
    }

    public static Thread getBlockedThread() throws InterruptedException {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                SynchronizedCounter.increment();
            }
        });
        t1.start();
        Thread.currentThread().sleep(1000);
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                SynchronizedCounter.increment();
            }
        });
        t2.start();
        Thread.currentThread().sleep(500);
        return t2;

    }

    public static Thread getWaitingThread() {
        final Integer i = 0;

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (i) {
                    try {
                        i.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });
        t.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return t;
    }

    public static Thread getTimedWaitingThread() {
        final Integer i = 0;

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (i) {
                    try {
                        i.wait(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });
        t.start();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        return t;
    }

    public static Thread getTerminatedThread() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
        t.start();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return t;
    }
}