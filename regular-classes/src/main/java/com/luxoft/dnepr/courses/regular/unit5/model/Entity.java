package com.luxoft.dnepr.courses.regular.unit5.model;


public class Entity {
    public Entity() {
    }

    public Entity(Long id) {

        this.id = id;
    }

    private Long id;
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }
}
