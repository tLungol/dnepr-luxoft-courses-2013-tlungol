package com.luxoft.dnepr.courses.regular.unit2;

public class Beverage extends AbstractProduct {
    private boolean nonAlcoholic;

    public boolean isNonAlcoholic() {
        return nonAlcoholic;
    }

    public void setNonAlcoholic(boolean nonAlcoholic) {
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o) ? nonAlcoholic == ((Beverage) o).isNonAlcoholic() : false;
    }


    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }

    @Override

    public int hashCode() {
        int result = super.hashCode();
        result = result + (nonAlcoholic ? 31 : 0);
        return result;
    }
}
