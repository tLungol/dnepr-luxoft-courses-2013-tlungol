package com.luxoft.dnepr.courses.regular.unit2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {
    private List<Product> childProducts = new ArrayList<Product>();
    private int amount;

    public CompositeProduct() {
    }

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    public String getCode() {
        return childProducts.isEmpty() ? null : childProducts.get(0).getCode();
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    public String getName() {
        return childProducts.isEmpty() ? null : childProducts.get(0).getName();
    }


    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    public double getPrice() {
        if (childProducts.isEmpty()) {
            return 0;
        }
        if (getAmount() >= 3) {
            return getSummary() * 0.9;
        }
        if (getAmount() == 2) {
            return getSummary() * 0.95;
        }
        return childProducts.size() * childProducts.get(0).getPrice();
    }

    public double getSummary() {
        double sum = 0;
        for (Product product : childProducts) {
            sum = sum + product.getPrice();
        }
        return sum;
    }

    public int getAmount() {
        return childProducts.size();
    }

    public void add(Product product) {
        childProducts.add(product);
    }

    public void remove(Product product) {
        childProducts.remove(product);
    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
