package com.luxoft.dnepr.courses.regular.unit4;


import java.util.*;

public class EqualSet<E>  implements Set<E>  {

    private boolean nullExists=false;
    private ArrayList<E> set ;


    public EqualSet() {
        set=new ArrayList<>();
    }
    public EqualSet(Collection<? extends E> collection) {
        this();
        addAll(collection);
    }

    @Override
    public int size() {
        return set.size();
    }

    @Override
    public boolean isEmpty() {
        return set.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return set.contains(o);
    }

    @Override
    public Iterator<E> iterator() {
        return set.iterator();
    }

    @Override
    public Object[] toArray() {
        return set.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return (T[]) set.toArray();
    }

    @Override
    public boolean add(E e) {
        if (e==null){
            if (!nullExists){
                this.nullExists=true;
            }
        }
        if (set.contains(e)){
            return false;
        }
        else{
            set.add((E) e);
            return true;
        }
    }

    @Override
    public boolean remove(Object o) {
        /*if (o==null){
            set.remove(null);
            this.nullExists=false;
        }*/
        if (set.contains(o)){

            set.remove(o);
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        if (set.containsAll(c)){
            return true;
        }
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean isChanged=false;
        ArrayList<E> in = (ArrayList<E>) c;
        Iterator<E> iterator=in.iterator();
        while (iterator.hasNext()){
            Object current =iterator.next();
            if (!set.contains(current)){
                isChanged=true;
                set.add((E) current);
            }
        }
        return isChanged;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return set.retainAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return set.removeAll(c);
    }

    @Override
    public void clear() {
        this.nullExists=false;
        set.clear();
    }
}
