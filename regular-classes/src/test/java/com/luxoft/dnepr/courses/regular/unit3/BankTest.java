package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


public class BankTest {


    @Test
    public void bankTest() {

        boolean thrown = false;

        try {
            Bank bank = new Bank("1.5");
        } catch (IllegalJavaVersionError e) {
            thrown = true;
        }

        assertTrue(thrown);
    }


    @Test
    public void testMakeMoneyTransaction() throws Exception {
        Bank bank = new Bank("1.7.0_07");
        Map<Long, UserInterface> users = new HashMap<>();
        User user = new User("Vasya", new Wallet(WalletStatus.ACTIVE, BigDecimal.valueOf(200), BigDecimal.valueOf(20)));
        users.put(user.getId(), user);
        user = new User("Petya", new Wallet(WalletStatus.ACTIVE, BigDecimal.valueOf(500), BigDecimal.valueOf(300)));
        users.put(user.getId(), user);
        bank.setUsers(users);
        bank.makeMoneyTransaction(Long.valueOf(2), Long.valueOf(1), BigDecimal.valueOf(10));
        assertEquals(bank.getUsers().get(Long.valueOf(1)).getWallet().getAmount(), BigDecimal.valueOf(20+10));
        assertEquals(bank.getUsers().get(Long.valueOf(2)).getWallet().getAmount(), BigDecimal.valueOf(290));
        try {
            bank.makeMoneyTransaction(Long.valueOf(2), Long.valueOf(1), BigDecimal.valueOf(410));
            fail();
        }catch (TransactionException e){
            assertEquals(e.getMessage(), "User 'Petya' has insufficient funds (290.00 < 410.00)");
        }

        try {
            bank.makeMoneyTransaction(Long.valueOf(2), Long.valueOf(1), BigDecimal.valueOf(290));
            fail();
        }catch (TransactionException e){
            assertEquals(e.getMessage(),"User 'Vasya' wallet limit exceeded (30.00 + 290.00 > 200.00)");
        }

        try {
            bank.makeMoneyTransaction(Long.valueOf(2), Long.valueOf(4), BigDecimal.valueOf(290));
            fail();
        }catch (NoUserFoundException e){
            assert(true);
        }


    }
}
