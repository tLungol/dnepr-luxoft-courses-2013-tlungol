package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Before;
import org.junit.Test;


import java.util.ArrayList;
import static org.junit.Assert.*;
import static junit.framework.Assert.*;


public class EqualSetTest {
    private EqualSet set;
    @Before
    public  void initialize(){
        set = new EqualSet();

    }
    @Test
    public void testAdd() throws Exception {
        assertEquals(set.add(null), true);
        assertEquals(set.add(null), false);
        assertEquals(set.add(Integer.valueOf(18)), true);
        assertEquals(set.add(Integer.valueOf(18)), false);
        assertEquals(set.size(), 2);
        ArrayList array =new ArrayList();
        array.add(Integer.valueOf(5));
        array.add(Integer.valueOf(5));
        array.add(null);
        array.add(Integer.valueOf(7));
        array.add(Integer.valueOf(10));
        array.add(null);
        set = new EqualSet(array);
        assertArrayEquals(set.toArray(),new Object[]{5,null,7,10});
        set.remove(null);
        set.add(null);
        assertArrayEquals(set.toArray(),new Object[]{5,7,10,null});
        array.clear();
        array.add(Integer.valueOf(7));
        array.add(Integer.valueOf(10));
        assertEquals(set.containsAll(array),true);
        set.retainAll(array);
        assertArrayEquals(set.toArray(),new Object[]{7,10});


    }
}
