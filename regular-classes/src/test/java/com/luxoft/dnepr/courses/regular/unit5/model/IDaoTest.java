package com.luxoft.dnepr.courses.regular.unit5.model;

import com.luxoft.dnepr.courses.regular.unit5.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import org.junit.Before;
import org.junit.Test;

import static com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage.getEntities;
import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.fail;


public class IDaoTest {
    private IDao<Redis> redisDao = new RedisDaoImpl();
    IDao<Employee> employeeIDao = new EmployeeDaoImpl();

    @Before
    public void initialize() throws UserAlreadyExist {
        getEntities().clear();
        redisDao.save(new Redis(Long.valueOf(12), 2));
        employeeIDao.save(new Employee(2000));
    }

    @Test
    public void testSave() throws Exception {
        try {
            redisDao.save(new Redis(Long.valueOf(12), 2));
            fail();
        } catch (UserAlreadyExist e) {
            assert (true);
        }
        try {
            employeeIDao.save(new Employee(Long.valueOf(12),2000));
            fail();
        } catch (UserAlreadyExist e) {
            assert (true);
        }
        employeeIDao.save(new Employee(null,2000));
        assertEquals(getEntities().size(), 3);
        assertEquals(employeeIDao.get(14).getId(),new Employee(Long.valueOf(14),2000).getId());


    }

    @Test
    public void testUpdate() throws Exception {
        employeeIDao.update(new Employee(Long.valueOf(13),5000));
        assertEquals(employeeIDao.get(13).getSalary(),new Employee(Long.valueOf(13),5000).getSalary());
        try {
            employeeIDao.update(new Employee(Long.valueOf(100), 2000));
            fail();
        } catch (UserNotFound e) {
            assert (true);
        }

        redisDao.update(new Redis(Long.valueOf(12),5000));
        assertEquals(redisDao.get(12).getWeight(),new Redis(Long.valueOf(13),5000).getWeight());
        try {
            redisDao.update(new Redis(Long.valueOf(100),2000));
            fail();
        } catch (UserNotFound e) {
            assert (true);
        }


    }

    @Test
    public void testGet() throws Exception {
        assertEquals(redisDao.get(1),null);
        assertEquals(employeeIDao.get(1),null);
        assertEquals(employeeIDao.get(13).getId(),Long.valueOf(13));
        assertEquals(redisDao.get(12).getId(),Long.valueOf(12));
    }

    @Test
    public void testDelete() throws Exception {
        assertEquals(employeeIDao.delete(15),false);
        assertEquals(employeeIDao.delete(13),true);
        assertEquals(redisDao.delete(12),true);
        assertEquals(redisDao.delete(1),false);
    }
}
