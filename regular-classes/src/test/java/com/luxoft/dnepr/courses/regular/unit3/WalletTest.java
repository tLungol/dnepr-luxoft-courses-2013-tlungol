package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;


public class WalletTest {
    User user;

    @Before
    public void initializeUser() {
        user = new User("Vasya", new Wallet(WalletStatus.ACTIVE, BigDecimal.valueOf(200), BigDecimal.valueOf(190)));
    }


    @Test
    public void testGetAmount() throws Exception {
        assertEquals(user.getWallet().getAmount(), BigDecimal.valueOf(190));
    }

    @Test
    public void testGetStatus() throws Exception {
        assertEquals(user.getWallet().getStatus(), WalletStatus.ACTIVE);
        user.getWallet().setStatus(WalletStatus.BLOCKED);
        assertEquals(user.getWallet().getStatus(), WalletStatus.BLOCKED);
    }


    @Test
    public void testGetMaxAmount() throws Exception {
        assertEquals(user.getWallet().getAmount(), BigDecimal.valueOf(190));

    }


    @Test
    public void testCheckWithdrawal() throws Exception {
        user.getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            user.getWallet().checkWithdrawal(BigDecimal.valueOf(200));
            fail();
        }catch (WalletIsBlockedException e){
            assert(true);
        }
        user.getWallet().setStatus(WalletStatus.ACTIVE);
        try {
            user.getWallet().checkWithdrawal(BigDecimal.valueOf(200));
            fail();
        }catch (InsufficientWalletAmountException e){
            assert(true);
        }
    }

    @Test
    public void testWithdraw() throws Exception {
        user.getWallet().setAmount(BigDecimal.valueOf(200));
        user.getWallet().withdraw(BigDecimal.valueOf(100));
        assertEquals(user.getWallet().getAmount(),BigDecimal.valueOf(100));

    }

    @Test
    public void testCheckTransfer() throws Exception {
        user.getWallet().setAmount(BigDecimal.valueOf(200));
        try {
            user.getWallet().checkTransfer(BigDecimal.valueOf(200.1));
            fail();
        }catch (LimitExceededException e){
            assert(true);
        }
        user.getWallet().setStatus(WalletStatus.BLOCKED);
        try {
            user.getWallet().checkWithdrawal(BigDecimal.valueOf(200));
            fail();
        }catch (WalletIsBlockedException e){
            assert(true);
        }

    }

    @Test
    public void testTransfer() throws Exception {
        user.getWallet().setAmount(BigDecimal.valueOf(200));
        user.getWallet().transfer(BigDecimal.valueOf(100));
        assertEquals(user.getWallet().getAmount(),BigDecimal.valueOf(300));

    }
}
