package com.luxoft.dnepr.courses.regular.unit6.storage;


import com.luxoft.dnepr.courses.regular.unit6.dao.EmployeeDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit6.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit6.model.Employee;
import com.luxoft.dnepr.courses.regular.unit6.model.Redis;
import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static com.luxoft.dnepr.courses.regular.unit6.storage.EntityStorage.getEntities;
import static junit.framework.Assert.assertEquals;


public class EntityStorageTest {
    private IDao<Redis> redisDao = new RedisDaoImpl();
    IDao<Employee> employeeIDao = new EmployeeDaoImpl();

    public class CreatingThread implements Runnable {
        public void run() {
            redisDao.save((new Redis(2)));
            employeeIDao.save(new Employee(2000));
        }

    }


    public class DelThread implements Runnable {
        public void run() {
            for (int i = 12; i <= 34; i++) {
                redisDao.delete(Long.valueOf(i));
            }

        }
    }

    public class UpdatingThread implements Runnable {
        public void run() {
            for (int i = 12; i <= 34; i++) {
                redisDao.update(new Redis(Long.valueOf(i), 100));
            }

        }

    }


    @Before
    public void initialize() {
        getEntities().clear();
        redisDao.save(new Redis(Long.valueOf(12), 2));
        employeeIDao.save(new Employee(2000));
    }


    @Test
    public void testGetNextId() throws Exception {
        Executor executor = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 10; i++) {
            executor.execute(new CreatingThread());
        }
        Thread.currentThread().sleep(1000);
        assertEquals(getEntities().size(), 22);

    }

    @Test
    public void deleteUpdate() throws Exception {
        Executor executor = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 20; i++) {
            redisDao.save((new Redis(2)));
        }
        try {
            executor.execute(new DelThread());
            executor.execute(new UpdatingThread());
        }catch (EntityNotFoundException e){

        }

        Thread.currentThread().sleep(1000);
        assertEquals(getEntities().size(), 0);
    }



}
