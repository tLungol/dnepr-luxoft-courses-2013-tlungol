package com.luxoft.dnepr.courses.regular.unit7_1;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ThreadProducerTest {
    @Test
    public void testGetNewThread() throws Exception {
        assertEquals(ThreadProducer.getNewThread().getState(), Thread.State.NEW);
    }

    @Test
    public void testGetRunnableThread() throws Exception {
        assertEquals(ThreadProducer.getRunnableThread().getState(), Thread.State.RUNNABLE);
    }

    @Test
    public void testGetBlockedThread() throws Exception {
        assertEquals(ThreadProducer.getBlockedThread().getState(), Thread.State.BLOCKED);

    }

    @Test
    public void testGetWaitingThread() throws Exception {
        assertEquals(ThreadProducer.getWaitingThread().getState(), Thread.State.WAITING);
    }

    @Test
    public void testGetTimedWaitingThread() throws Exception {
        assertEquals(ThreadProducer.getTimedWaitingThread().getState(), Thread.State.TIMED_WAITING);
    }

    @Test
    public void testGetTerminatedThread() throws Exception {
        assertEquals(ThreadProducer.getTerminatedThread().getState(), Thread.State.TERMINATED);
    }
}
