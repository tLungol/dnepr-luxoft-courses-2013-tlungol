package com.luxoft.dnepr.courses.regular.unit8;

import com.google.gson.Gson;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Date;

import static junit.framework.TestCase.assertEquals;


public class SerializerTest {
    private FamilyTree obj;
    @Before
    public void initialize() throws Exception {
        Person person = new Person();
        person.setBirthDate(new Date());
        person.setEthnicity("ukrainian");
        person.setGender(Gender.FEMALE);
        person.setName("Taras");
        Person mother = new Person();
        mother.setName("Natali");
        Person father = new Person();
        mother.setGender(Gender.FEMALE);
        father.setEthnicity("ukrainian");
        father.setGender(Gender.MALE);
        father.setName("Alexandr");
        Person grandMa = new Person();
        grandMa.setName("Nina");
        grandMa.setEthnicity("russian");
        grandMa.setBirthDate(new Date());
        grandMa.setGender(Gender.FEMALE);
        Person grandPa= new Person();
        grandPa.setName("Valentin");
        grandPa.setGender(Gender.MALE);
        father.setFather(grandPa);
        father.setMother(grandMa);
        person.setMother(mother);
        person.setFather(father);




        obj = new FamilyTree(person);
    }
    @Test
    public void testSerialize() throws Exception {


    }

    @Test
    public void testDeserialize() throws Exception {
        Serializer.serialize(new File("family.txt"),obj);
        BufferedReader   input= new BufferedReader(new FileReader("family.txt"));
        FamilyTree ft =   Serializer.deserialize(new File("family.txt"));
        Serializer.serialize(new File("deserialized.txt"),ft);
        assertEquals(ft.getRoot().getName(),obj.getRoot().getName());
        assertEquals(ft.getRoot().getFather().getName(),obj.getRoot().getFather().getName());
        assertEquals(ft.getRoot().getFather().getMother().getName(),obj.getRoot().getFather().getMother().getName());
        assertEquals(ft.getRoot().getFather().getFather().getName(),obj.getRoot().getFather().getFather().getName());
        assertEquals(ft.getRoot().getFather().getFather().getBirthDate(),obj.getRoot().getFather().getFather().getBirthDate());
        BufferedReader in1 = new BufferedReader(new FileReader("deserialized.txt"));
        BufferedReader in2 = new BufferedReader(new FileReader("family.txt"));
        assertEquals(in1.readLine(),in2.readLine());


    }
}
