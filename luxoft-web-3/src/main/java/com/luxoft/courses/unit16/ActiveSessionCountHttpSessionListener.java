package com.luxoft.courses.unit16;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

public class ActiveSessionCountHttpSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndIncrement();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndDecrement();
        if (((String)hse.getSession().getAttribute("role")).equals("admin")){
            getActiveAdminSessions(hse).getAndDecrement();
        } else {
            if (((String)hse.getSession().getAttribute("role")).equals("user")){
                getActiveUserSessions(hse).getAndDecrement();
            }
        }
    }

    private AtomicLong getActiveSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
    private AtomicLong getActiveUserSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_USER_SESSION_ATTRIBUTE);
    }
    private AtomicLong getActiveAdminSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_ADMIN_SESSION_ATTRIBUTE);
    }
    private AtomicLong getHTTPCount(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.HTTP_REQUEST_COUNT);
    }
}
