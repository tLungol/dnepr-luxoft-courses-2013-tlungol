package com.luxoft.courses.unit16;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_ADMIN_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_USER_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_REQUEST_COUNT, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_GET_REQUEST_COUNT, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_POST_REQUEST_COUNT, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_OTHER_REQUEST_COUNT, new AtomicLong(0));

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
}
