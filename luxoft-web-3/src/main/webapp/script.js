$(document).ready(function() {
    $("button").bind('click', function () {
        $.post("login", { name: $("#name").val(), password: $('#password').val() },
            function(data){
                if(data == "user") {
                    location.href = "user";
                }
                else if(data == "admin") {
                    location.href = "admin/sessionData";
                }else{
                    $(".error").empty();
                    $("#name").after('<span class = "error">'+ data+'</span>' );}

            });
    });
});
