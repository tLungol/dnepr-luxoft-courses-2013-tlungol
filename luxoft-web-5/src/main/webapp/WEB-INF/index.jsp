<%--
  Created by IntelliJ IDEA.
  User: bulick
  Date: 21.06.13
  Time: 8:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>Login</title>
</head>
<body>
<div class="login">
    <form method="post" action="login">
        <table>
            <tr>
                <td>Login</td><td><input  name="name" type="text"/></td>
                <td><%= request.getParameter("error")!=null ? "<div class=\"error\">Wrong login or password</div>": ""%>
                   </td>
            </tr>
            <tr>
                <td>Password</td><td><input name="password" type="password"/></td>
            </tr>
            <tr><td></td><td><input type="submit" value="Sign in"></td></tr>
        </table>

    </form>
</div>
</body>
</html>