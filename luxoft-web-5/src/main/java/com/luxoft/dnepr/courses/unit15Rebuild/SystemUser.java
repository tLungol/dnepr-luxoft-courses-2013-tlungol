package com.luxoft.dnepr.courses.unit15Rebuild;

/**
 * Created with IntelliJ IDEA.
 * User: bulick
 * Date: 15.06.13
 * Time: 9:41
 * To change this template use File | Settings | File Templates.
 */
public class SystemUser {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SystemUser that = (SystemUser) o;

        if (!login.equals(that.login)) return false;
        if (!password.equals(that.password)) return false;
        if (!role.equals(that.role)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return login.hashCode();
    }

    public SystemUser(String login, String password, String role) {
        this.login = login;
        this.password = password;
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    private String login;
    private String password;
    private String role;

}
