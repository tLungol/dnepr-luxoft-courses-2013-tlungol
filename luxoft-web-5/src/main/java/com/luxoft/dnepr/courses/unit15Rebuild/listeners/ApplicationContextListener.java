package com.luxoft.dnepr.courses.unit15Rebuild.listeners;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit15Rebuild.Constants;
import com.luxoft.dnepr.courses.unit15Rebuild.SystemUser;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        sce.getServletContext().setAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_ADMIN_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.ACTIVE_USER_SESSION_ATTRIBUTE, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_REQUEST_COUNT, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_GET_REQUEST_COUNT, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_POST_REQUEST_COUNT, new AtomicLong(0));
        sce.getServletContext().setAttribute(Constants.HTTP_OTHER_REQUEST_COUNT, new AtomicLong(0));

        Gson gson = new Gson();
        Type list = new TypeToken<List<SystemUser>>() {
        }.getType();
        ArrayList<SystemUser> users =
                gson.fromJson(sce.getServletContext().getInitParameter("users"), list);
        sce.getServletContext().setAttribute(Constants.USER_ARRAY, users);

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().removeAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }
}
