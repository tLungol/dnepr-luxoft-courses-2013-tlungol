package com.luxoft.dnepr.courses.unit15Rebuild.listeners;

import com.luxoft.dnepr.courses.unit15Rebuild.Constants;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;

public class ActiveSessionCountHttpSessionListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndIncrement();
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        getActiveSessions(hse).getAndDecrement();

    }

    private AtomicLong getActiveSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_SESSION_ATTRIBUTE);
    }

    private AtomicLong getHTTPCount(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.HTTP_REQUEST_COUNT);
    }
}
