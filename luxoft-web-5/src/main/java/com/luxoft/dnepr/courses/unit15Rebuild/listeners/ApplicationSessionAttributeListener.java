package com.luxoft.dnepr.courses.unit15Rebuild.listeners;

import com.luxoft.dnepr.courses.unit15Rebuild.Constants;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import java.util.concurrent.atomic.AtomicLong;


public class ApplicationSessionAttributeListener implements HttpSessionAttributeListener {


    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (event.getName().equals("role")) {
            if (((String) event.getValue()).equals("admin")) {
                getActiveAdminSessions(event).getAndIncrement();
            } else {
                if (((String) event.getValue()).equals("user")) {
                    getActiveUserSessions(event).getAndIncrement();
                }
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (event.getName().equals("role")) {
            if (((String) event.getValue()).equals("admin")) {
                getActiveAdminSessions(event).getAndDecrement();
            } else {
                if (((String) event.getValue()).equals("user")) {
                    getActiveUserSessions(event).getAndDecrement();
                }
            }
        }
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
    }

    private AtomicLong getActiveUserSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_USER_SESSION_ATTRIBUTE);
    }

    private AtomicLong getActiveAdminSessions(HttpSessionEvent hse) {
        return (AtomicLong) hse.getSession().getServletContext().getAttribute(Constants.ACTIVE_ADMIN_SESSION_ATTRIBUTE);
    }

}
