package com.luxoft.dnepr.courses.unit15Rebuild.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {

    private  ServletContext context;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        context = filterConfig.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        if (session == null) {
            response.sendRedirect(context.getContextPath() + "/");
        } else if (session.getAttribute("role") != null && ((String) session.getAttribute("role")).equals("user")) {
            response.sendRedirect(context.getContextPath() + "/user");
        } else {
            filterChain.doFilter(request, response);
        }

    }


    @Override
    public void destroy() {

    }
}
