package com.luxoft.dnepr.courses.unit15Rebuild.servlets;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luxoft.dnepr.courses.unit15Rebuild.Constants;
import com.luxoft.dnepr.courses.unit15Rebuild.SystemUser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String role;
        out.print(request.getContextPath());
        if ((role = check(request.getParameter("name"), request.getParameter("password"))) != null) {
            HttpSession session = request.getSession();
            session.setAttribute("role", role);
            session.setAttribute("login", request.getParameter("name"));
            if (role.equals("user")) {
                response.sendRedirect(request.getContextPath() + "/user");
            } else if (role.equals("admin")) {
                response.sendRedirect(request.getContextPath() + "/admin/sessionData");
            }
        } else {
            response.sendRedirect(request.getContextPath() + "?error");
        }

    }

    private boolean checkParam(String param) {
        if (param != null && param != "")
            return true;
        return false;
    }

    private String check(String userName, String pass) {

        List<SystemUser> users = (List<SystemUser>) getServletContext().getAttribute(Constants.USER_ARRAY);
        for (SystemUser user : users) {
            if (checkParam(userName) && checkParam(pass) &&
                    user.getLogin().equals(userName) && user.getPassword().equals(pass)) {
                return user.getRole();
            }
        }
        return null;
    }
}
