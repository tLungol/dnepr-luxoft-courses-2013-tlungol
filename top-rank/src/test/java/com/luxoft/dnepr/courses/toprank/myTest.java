package com.luxoft.dnepr.courses.toprank;



import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static junit.framework.Assert.assertEquals;

public class myTest {
   /* @Test
    public void myTest() throws Exception {
        Pattern p = Pattern.compile("<a href=\"(.*?)\">");
        String s ="<a href=\"http://udacity.com/cs101x/urank/nickel.html\">the Nickel Chef</a>.\n";
        Matcher m = p.matcher(s);
        while (m.find()){
            System.out.println(m.group(1));
        }


    }    */
   @Test
   public void myTest() throws Exception {
       TopRankExecutor executor = new TopRankExecutor(0.8, 2);
       Map<String, String> urlContent = new HashMap();
       urlContent.put("http://no_address/A.html","<html>\n" +
               "<body>\n" +
               "<h1>Page A</h1>\n" +
               "<a href=\"http://no_address/B.html\">B</a>\n" +
               "<a href=\"http://no_address/C.html\">C</a>\n" +
               "</body>\n" +
               "</html>");
       urlContent.put("http://no_address/B.html","<html>\n" +
               "<body>\n" +
               "<h1>Page B</h1>\n" +
               "<a href=\"http://no_address/C.html\">C</a>\n" +
               "</body>\n" +
               "</html>");
       urlContent.put("http://no_address/C.html","<html>\n" +
               "<body>\n" +
               "<h1>Page C</h1>\n" +
               "</body>\n" +
               "</html>");
      // urlContent.put("","");
       TopRankResults results = executor.execute(urlContent);
      // Assert.assertEquals(0.033333333333333326d, results.getRanks().get("http://udacity.com/cs101x/urank/index.html"), ERROR);
       assertEquals(0.0666667, results.getRanks().get("http://no_address/A.html"),0.0000001);
       assertEquals(0.09333338, results.getRanks().get("http://no_address/B.html"),0.0000001);
       assertEquals(0.2533333, results.getRanks().get("http://no_address/C.html"),0.0000001);

   }


   }


