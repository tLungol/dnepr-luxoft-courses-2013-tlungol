package com.luxoft.dnepr.courses.toprank;


import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TopRankExecutor {
    private double dampingFactor;
    private int numberOfLoopsInRankComputing;
    private ExecutorService taskExecutor = Executors.newFixedThreadPool(10);
    private TopRankResults results = new TopRankResults();
    private Map<String, String> urlContent;
    public TopRankExecutor(double dampingFactor, int numberOfLoopsInRankComputing) {
        this.dampingFactor = dampingFactor;
        this.numberOfLoopsInRankComputing = numberOfLoopsInRankComputing;
    }

    public TopRankResults execute(Map<String, String> urlContent) {
        this.urlContent=urlContent;
        Iterator<String> iterator = urlContent.keySet().iterator();
        String url="";
        int numberOfPages = urlContent.size();
        while (iterator.hasNext()) {
            url= iterator.next();
            results.getGraph().put(url, new ArrayList<String>());
            results.getRanks().put(url, 1.0 / numberOfPages);
            fillIndex(url, urlContent.get(url).split("[\t\n\\s]"));
            fillReversGraph(url);

        }
        fillGraph(urlContent);
        taskExecutor.shutdown();
        try {
            taskExecutor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("Process interrupted!");
        }
        calculateRank(urlContent);
        return results;
    }

    private void fillReversGraph(final String url) {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                    Pattern p = Pattern.compile("<a href=\"(.*?)\">");
                    Matcher m = p.matcher(urlContent.get(url));
                    List<String> array = new ArrayList<String>();
                    while (m.find()) {
                        array.add(m.group(1));
                    }
                    results.getReverseGraph().put(url, array);
                }
        });
    }


    private void fillGraph(final Map<String, String> urlContent) {
        taskExecutor.execute(new Runnable() {

            @Override
            public void run() {
                Iterator<String> i = urlContent.keySet().iterator();
                List<String> array;
                while (i.hasNext()) {
                    String url = i.next();
                    Pattern p = Pattern.compile("<a href=\"(.*?)\">");
                    Matcher m = p.matcher(urlContent.get(url));
                    while (m.find()) {
                        array = results.getGraph().get(m.group(1));
                        array.add(url);
                        results.getGraph().put(m.group(1), array);
                    }
                }
            }
        });
    }



    private double sumForRank(double dampingFactor, Map<String, Double> previousRanks, TopRankResults results, String page, Map<String, String> urlContent) {
        double sum = 0;
        List<String> referencesFrom = results.getGraph().get(page);
        Iterator<String> i = referencesFrom.iterator();
        double previousRank;
        String referance;
        while (i.hasNext()) {
            referance = i.next();
            previousRank = previousRanks.get(referance);
            Pattern p = Pattern.compile("<a href=\"(.*?)\">");
            Matcher m = p.matcher(urlContent.get(referance));
            int counter = 0;
            while (m.find()) {
                counter++;
            }
            if (counter == 0)
                continue;

            sum = sum + dampingFactor * previousRank / counter;
        }

        return sum;
    }

    private void fillIndex(final String url, final String[] words) {
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                List<String> array;
                for (String word : words) {
                    if (results.getIndex().containsKey(word)) {
                        array = results.getIndex().get(word);
                        array.add(url);
                        results.getIndex().put(word, array);
                    } else {
                        array = new ArrayList<String>();
                        array.add(url);
                        results.getIndex().put(word, array);
                    }
                }
            }
        });
    }

    private void calculateRank(Map<String, String> urlContent) {
        Iterator<String> iterator;
        String url;
        int numberOfPages = urlContent.keySet().size();
        Map<String, Double> previousRanks;
        double rank;
        for (int i = 0; i < numberOfLoopsInRankComputing; i++) {
            previousRanks = (Map) ((HashMap) (results.getRanks())).clone();
            iterator = urlContent.keySet().iterator();
            while (iterator.hasNext()) {
                url = iterator.next();
                rank = (1 - dampingFactor) / numberOfPages + sumForRank(dampingFactor, previousRanks, results, url, urlContent);
                results.getRanks().put(url, rank);
            }
        }

    }


}
