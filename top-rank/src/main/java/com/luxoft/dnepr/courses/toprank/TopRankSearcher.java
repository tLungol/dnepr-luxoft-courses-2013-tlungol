package com.luxoft.dnepr.courses.toprank;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class TopRankSearcher {
    private static final double DEFAULT_DAMPING_FACTOR = 0.8;
    private static final int DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING = 10;
    private static Map<String, String> toExecute;

    public TopRankResults execute(List<String> urls) {
        return execute(urls,DEFAULT_DAMPING_FACTOR,DEFAULT_NUMBER_OF_LOOPS_IN_RANK_COMPUTING);
    }

    public TopRankResults execute(List<String> urls, double dampingFactor, int numberOfLoopsInRankComputing) {
        toExecute = new ConcurrentHashMap<String, String>();
        ExecutorService taskExecutor = Executors.newCachedThreadPool();
        for (final String url: urls){
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        toExecute.put(url, getFromUrl(url));
                    } catch (MalformedURLException e) {
                        System.err.println("Malformed URL:\""+url+"\"");
                    }
                }
            });
        }
        taskExecutor.shutdown();
        try {
            taskExecutor.awaitTermination(10,TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            System.err.println("Process interrupted!");
        }

        TopRankExecutor topRankExecutor =
                new TopRankExecutor(dampingFactor, numberOfLoopsInRankComputing);
        return topRankExecutor.execute(toExecute);
    }

    private String getFromUrl(String url) throws MalformedURLException {
        URL currentURL = new URL(url);
        URLConnection connection = null;
        BufferedReader in = null;
        String result = "";
        try {
            connection = currentURL.openConnection();
            in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine = "";
            while ((inputLine = in.readLine()) != null) {
                result += inputLine;
            }
            ;

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally {
            assert in != null;
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

}

