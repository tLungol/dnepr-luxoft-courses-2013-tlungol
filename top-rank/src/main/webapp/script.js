/**
 * Created with IntelliJ IDEA.
 * User: bulick
 * Date: 06.06.13
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */
jQuery(function () {


    $("input:text").bind('click', function (e) {
        if ($("input:text").val() === "Please, enter the search string") {
            $("input:text").val("");
            $("input").css( "color", "black" );
        }
    });

    $("input:text").bind('blur', function () {
        if ($("input:text").val() === "") {
            $("input").css( "color", "grey" );
            $("input:text").val("Please, enter the search string");

        }
    });
    $("button").bind('click', function () {
        $(".results").empty();
        var dummyData = '[{ "href": "jquery.com", "description": "JQuery: very popular lightweight javascript library"},{ "href" : "w3.org", "description" : "World wide web consorcium main web site" },{ "href" : "mozilla.org", "description" : "Mozilla foundation resources" },{ "href" : "ru.wikipedia.org", "description" : "Russian division of wikipedia - free internet enciclopedia" }]';
        var obj = $.parseJSON(dummyData);
        var result = '';
        $.each(obj, function() {
             result+='<div class="result"><a href="'+ this['href']+'">' + this['href'] +'</a><p>'+ this['description']+'</p></div>';

        });
       $('.results').html(result);
    });
})($)

