SELECT
  printer.model,
  printer.price
FROM printer
WHERE price IN (SELECT
                  max(price)
                FROM printer)
