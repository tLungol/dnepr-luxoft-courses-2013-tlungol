SELECT
  makers.maker_id,
  count(product.model) AS mod_count
FROM product, makers
WHERE product.maker_id = makers.maker_id AND product.type = 'PC'
GROUP BY maker_id
HAVING mod_count >= 3
  