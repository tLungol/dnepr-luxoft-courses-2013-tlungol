SELECT
  'Laptop' AS type,
  laptop.model,
  laptop.speed
FROM pc, laptop
HAVING speed < min(pc.speed)