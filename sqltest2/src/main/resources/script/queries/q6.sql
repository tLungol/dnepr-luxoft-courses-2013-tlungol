SELECT
  makers.maker_name
FROM product, makers
WHERE makers.maker_id = product.maker_id
      AND product.type = 'PC'
      AND maker_name NOT IN (SELECT
                               makers.maker_name
                             FROM makers, product
                             WHERE product.type = 'Laptop'
                                   AND makers.maker_id = product.maker_id)
GROUP BY maker_name
