SELECT
  makers.maker_name
FROM makers, product
HAVING product.maker_id = makers.maker_id
       AND (select count(model) from product where maker_id=makers.maker_id)=1