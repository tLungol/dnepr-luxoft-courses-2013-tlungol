SELECT
  product.maker_id,
  AVG(laptop.screen) AS avg_size
FROM laptop, product
WHERE laptop.model = product.model
GROUP BY maker_id
ORDER BY avg_size