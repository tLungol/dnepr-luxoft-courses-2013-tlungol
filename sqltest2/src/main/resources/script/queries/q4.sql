SELECT makers.maker_name
FROM makers INNER JOIN product
    ON product.maker_id=makers.maker_id
WHERE product.type='Printer'
ORDER BY maker_name DESC