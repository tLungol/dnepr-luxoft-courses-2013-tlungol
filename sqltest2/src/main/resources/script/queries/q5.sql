SELECT
  product.model,
  pc.price
FROM product, pc, makers
WHERE product.model = pc.model AND product.maker_id = makers.maker_id AND maker_name = 'B'
GROUP BY model
UNION
SELECT
  product.model,
  printer.price
FROM product, printer, makers
WHERE product.model = printer.model AND product.maker_id = makers.maker_id AND maker_name = 'B'
GROUP BY model
UNION
SELECT
  product.model,
  laptop.price
FROM product, laptop, makers
WHERE product.model = laptop.model AND product.maker_id = makers.maker_id AND maker_name = 'B'
GROUP BY model