SELECT
  makers.maker_name,
  printer.price
FROM makers, printer, product
WHERE makers.maker_id = product.maker_id AND printer.model = product.model AND printer.color = 'y'
HAVING price = min(printer.price)