SELECT
  makers.maker_name,
  pc.speed
FROM product, makers, pc
WHERE product.model = pc.model AND product.maker_id = makers.maker_id AND pc.hd < 10
ORDER BY maker_name DESC