SELECT
  makers.maker_name
FROM makers, product, pc
WHERE pc.speed > 450 AND pc.model = product.model AND product.maker_id = makers.maker_id
GROUP BY maker_name;