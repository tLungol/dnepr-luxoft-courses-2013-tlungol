package com.luxoft.courses.unit13;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.Map;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        if (check(request.getParameter("name"), request.getParameter("password"))) {
            HttpSession session = request.getSession();
            response.addCookie(new Cookie("name", request.getParameter("name")));
            out.print("no_error");
        } else {
            out.print("Wrong Login or password");
        }

    }

    private boolean checkParam(String param) {
        if (param != null && param != "")
            return true;
        return false;
    }

    private boolean check(String userName, String pass) {
        Gson gson = new Gson();
        Type mapType = new TypeToken<Map<String, String>>() {
        }.getType();
        Map<String, String> map =
                gson.fromJson(getServletContext().getInitParameter("users"), mapType);
        if (checkParam(userName) && map.containsKey(userName))
            if (checkParam(pass) && map.get(userName).equals(pass))
                return true;
        return false;
    }
}
