package com.luxoft.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;


public class DummyServlet extends HttpServlet {
    private static Map<String, String> storrage = new HashMap<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            java.io.IOException {
        PrintWriter out = resp.getWriter();
        if ((!req.getParameterMap().containsKey("name")) || (!req.getParameterMap().containsKey("age"))) {
            resp.setStatus(500);
            resp.setContentType("application/json; charset=utf-8");
            String outStr = "\"error\": \"Illegal parameters\"";
            out.println(outStr);
            return;
        }
        if (!storrage.containsKey(req.getParameterValues("name")[0])) {
            resp.setStatus(500);
            resp.setContentType("application/json; charset=utf-8");
            String outStr = "{\"error\": \"Name " + req.getParameterValues("name")[0] + " does not exist\"}";
            out.println(outStr);
            return;
        }
        storrage.put(req.getParameterValues("name")[0], req.getParameterValues("age")[0]);
        resp.setStatus(202);

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            java.io.IOException {
        PrintWriter out = resp.getWriter();

        if ((!req.getParameterMap().containsKey("name")) || (!req.getParameterMap().containsKey("age"))) {
            resp.setStatus(500);
            resp.setContentType("application/json; charset=utf-8");
            String outStr = "\"error\": \"Illegal parameters\"";
            out.println(outStr);
            return;
        }
        if (storrage.containsKey(req.getParameterValues("name")[0])) {
            resp.setStatus(500);
            resp.setContentType("application/json; charset=utf-8");
            String outStr = "{\"error\": \"Name " + req.getParameterValues("name")[0] + " already exists\"}";
            out.println(outStr);
            return;
        }
        storrage.put(req.getParameterValues("name")[0], req.getParameterValues("age")[0]);
        resp.setStatus(201);
    }

}


