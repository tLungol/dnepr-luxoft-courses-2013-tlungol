package com.luxoft.courses.unit13;
import java.io.*;
import java.util.concurrent.atomic.AtomicInteger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class myServlet extends HttpServlet {
    private static int counter =0;
    private synchronized void incCounter(HttpServletResponse response) throws IOException {
        counter++;
        String outStr;
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(200);
        PrintWriter out = response.getWriter();
        outStr= "{\"hitCount\": "+counter+"}";
        out.println(outStr);
        response.setContentLength(outStr.length());
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        incCounter(response);
    }

}
