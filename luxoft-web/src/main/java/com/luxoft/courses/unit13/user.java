package com.luxoft.courses.unit13;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;


public class User extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        PrintWriter out = response.getWriter();
        if (session!=null){
            Cookie[] cookies = request.getCookies();
            if(request.getQueryString()!=null&&request.getQueryString().equals("logout")){
                session.invalidate();
                response.sendRedirect("index.html");
            }
            for(Cookie cookie:cookies){
                if (cookie.getName().equals("name")){
                    out.println("<html><link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\"/>" +
                            "<div class=\"Login\">Hello "+cookie.getValue()+"!</div>" +
                            "<div class\"logout\"><a href='User?logout'>logout</a></div></html>");
                    return;
                }
            }
        }

    }
}
